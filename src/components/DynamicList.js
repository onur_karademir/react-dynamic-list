import React, { Component,useState } from "react";
import { Container, Row, Col, Alert, Input, Button, InputGroup,InputGroupAddon } from "reactstrap";
import shoping from './shoping.png'
class DynamicList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buyItems: [
        { item: "egg", id: 0 },
        { item: "apple", id: 1 },
      ],
      item: "",
    };
  }
  submitHendler = (e) => {
      e.preventDefault();
      if (this.state.item.length === 0) {

      }else{
        let neoItem = {
          item:this.state.item
      }
      let setItem = [...this.state.buyItems,neoItem]
      this.setState ({
          buyItems:[...setItem],
          item : ""
      })
      }
     
  } 

  inputHendler = (e) => {
      this.setState({
          item:e.target.value
      })
  }

  deleteHendler = (item) => {
      const deleteElement = this.state.buyItems.filter(element=> {
            return item!==element
      });
      this.setState({
          buyItems:[...deleteElement]
      })
  }
  
  render() {
    return (
      <Container>
        <Row>
          <Col className="dynamic-items" xs="12" sm="12" md="6">
            <div className="img-div">
            <img src={shoping} class="img-responsive w-100" alt="Image"></img>
            </div>
          
            {/* input area */}
            <form onSubmit={this.submitHendler}>
              <InputGroup>
              <Input 
                onChange={this.inputHendler}
                type="text"
                placeholder="Add Item"
                value={this.state.item}
              />
              <InputGroupAddon addonType="append"><Button color="primary">Add Item</Button></InputGroupAddon>
              </InputGroup>
            </form>
            {
                this.state.buyItems.length === 0 && <AlertExample></AlertExample>
            }
            {/* item area */}
            {this.state.buyItems.map((item, index) => {
              return (
                <Container className="mt-4">
                  <Row className="p-3">
                    <Col xs="12" sm="12" md="6" key={index}>
                      <h5>{item.item}</h5>
                    </Col>
                    <Col xs="12" sm="12" md="6" key={index}>
                      <Button size="sm" color="danger" onClick={(e) => this.deleteHendler(item)}>
                        Delete Item
                      </Button>
                    </Col>
                  </Row>
                </Container>
              );
            })}
          </Col>
        </Row>
      </Container>
    );
  }
}

const AlertExample = (props) => {
  const [alertOpen,alertClose] = useState(true)
   const closeHendler = () => {
        alertClose(false)
    }
    return (
      <Alert color="danger" isOpen={alertOpen} toggle={closeHendler}>
       All item deleted my frend
      </Alert>
    );
  }

export default DynamicList;
